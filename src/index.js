import App from "./App";
import { render } from '@wordpress/element';

/**
 * Import the stylesheet for the plugin.
 */
import { Provider } from "react-redux";
import store from "./redux/store";

// Render the App component into the DOM
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('students-list')
);