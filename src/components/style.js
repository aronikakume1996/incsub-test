import styled from "styled-components";

const Button = styled.button`
    border-radius: 12px;
    color: #050d46 !important;
    font-family: Montserrat;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: 30px;
    padding: 10px 40px;
    border: none;
    background-color: #FFC913;
    cursor: pointer;
`

const AddButton = styled.button`
width: 50px;
height: 50px;
border: none;
border-radius: 50px;
background: #020732;
box-shadow: 0px 0px 35px #0A1148;
color: #fff;
font-size: 24px;
font-weight: bolder;
cursor: pointer;
display: flex;
justify-content: center;
align-items: center;
float:right;
margin: 10px 0px;
`



const Error = styled.span`
	color: red;
	font-size: 14px;
	padding: 20px 0px;
`;

const Span = styled.span`
color:#fff;
font-size: 20px;
font-weight: bold;
margin-right: 10px;
`

const BackDropOverlay = styled.div`
width: 100%;
height: 100%;
background: rgba(0,0,0,0.5);
z-index: 10;
position: fixed;
top:0;
left:0;
bottom:0;
display: flex;
justify-content: center;
align-items: center;
color: #fff;
`
const FormContainer = styled.div`
position: relative;
    max-width: 400px;
    height: auto;
    background: #020732;
    box-shadow: 0px 0px 35px #0A1148;
    width: 100%;
    border-radius: 25px;
    padding: 80px 20px 60px 20px;
    position: fixed;
    display:flex;
    align-items: center;
    flex-direction: column
`;

const Input = styled.input`
display: flex;
width: 100%;
padding: 21px 21px 21px 20px;
align-items: center;
border-radius: 15px;
color: #FFFFFF;
margin-bottom: 15px;
background: linear-gradient(92.34deg, #080c2f -17.36%, rgba(81, 90, 161, 0) 62.98%, #080c2f 141.67%, #080c2f 141.67%) padding-box, linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) padding-box, linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) border-box;
border: 2px solid transparent;
}
`
const Select = styled.select`
display: flex;
width: 100%;
padding: 21px 21px 21px 20px;
align-items: center;
border-radius: 15px;
color: #FFFFFF;
margin-bottom: 15px;
background: linear-gradient(92.34deg, #080c2f -17.36%, rgba(81, 90, 161, 0) 62.98%, #080c2f 141.67%, #080c2f 141.67%) padding-box, linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) padding-box, linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) border-box;
border: 2px solid transparent;
`;

const Option = styled.option`
color: #020732
`;

const TableContainer = styled.div`
  width: 100%;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 10px;
`;
const TableHeader = styled.th`
  background-color: #f2f2f2;
  padding: 10px;
  text-align: left;
  background: linear-gradient(92.34deg, #080c2f -17.36%, rgba(81, 90, 161, 0) 62.98%, #080c2f 141.67%, #080c2f 141.67%) padding-box,linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) padding-box,linear-gradient(92.84deg, #515aa1 -21.63%, #0a1148 44.82%, #515aa2 125.88%) border-box;
    border: 2px solid transparent;
    color: #fff;
`;

const TableCell = styled.td`
  padding: 10px;
  border: 1px solid #ddd;
`;

const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f9f9f9;
  }
`;

const CloseDiv = styled.div`
position: absolute;
top: 0;
right: 0;
width: 50px;
height: 50px;
border: none;
border-radius: 50px;
background: red;
box-shadow: 0px 0px 35px #0A1148;
color: #fff;
font-size: 24px;
font-weight: bolder;
cursor: pointer;
display: flex;
justify-content: center;
align-items: center;
float: right;
`;

const CloseIcon = styled.span`
  color: #ffc913;
  transform: rotate(45deg);
  display: flex;
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`

export { Select,Option,CloseDiv, CloseIcon, Button, AddButton, Error, Span, BackDropOverlay, FormContainer, Input, TableContainer, Table, TableHeader, TableCell, TableRow };