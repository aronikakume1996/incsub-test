import React, { useState } from 'react';
import { BackDropOverlay, Button, CloseDiv, CloseIcon, Error, FormContainer, Input, Option, Select, Span } from './style';
import { addStudent, closeAddForm } from '../redux/actions/students';
import { useDispatch } from 'react-redux';


const AddStudent=()=>{
    const [student, setStudent] = useState({ gender: 'Male' });
    const [error, setError] = useState('');
    const dispatch = useDispatch();
    const handleChange = (e) => {
		setStudent({
			...student,
			[e.target.name]: e.target.value,
		});
	};
    const handelSubmit = (e) => {
        e.preventDefault();
        const {fname, lname, cgpa, gender} = student;
        if(fname && lname && cgpa){
            dispatch(addStudent(student));
            console.log(student);
            	setError('');

        }else{
            	setError('Please Fill First!');
            console.log('Please enter');
        }
	};
    return(
        <BackDropOverlay>

            <FormContainer>
                <CloseDiv  onClick={() => dispatch(closeAddForm())}>
                    
                        <CloseIcon>
                            +
                        </CloseIcon>
                    
                </CloseDiv>
                <Input onChange={handleChange} name='fname' id='fname' className='fname' placeholder='First Name' />
                <Input onChange={handleChange} name='lname' id='lname' className='lname' placeholder='Last Name' />
                <Input onChange={handleChange} name='cgpa' id='cgpa' className='cgpa' placeholder='3.65' />
                <Select onChange={handleChange} name='gender' id='gender' className='gender'>
                    <Option value='Male'>Male</Option>
                    <Option value='Female'>Female</Option>
                    <Option value='Other'>Other</Option>
                </Select>

                <Error>{error}</Error>
                <Button type='button' onClick={(e)=>handelSubmit(e)} style={{width:"100%", padding: "15px 40px"}}>
                    <Span>+</Span>
                    Add Studentttt
                </Button>

            </FormContainer>
        </BackDropOverlay>
    )
}

export default AddStudent;