import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addForm, getStudents } from '../redux/actions/students';
import { AddButton, Input, Table, TableCell, TableContainer, TableHeader, TableRow } from './style';
import AddStudent from './AddStudent';
import { Pagination } from '@mui/material';


const Students = () => {
    const dispatch = useDispatch();
    const students = useSelector(state => state.students.students);
    const loading = useSelector(state => state.students.loading);
    const error = useSelector(state => state.students.error);
    const openAddForm = useSelector(state => state.students.openAddForm);
    const [page, setPage] = useState(1);


    useEffect(() => {
        dispatch(getStudents({ page: page }));
    }, []);
 
    const handlePageChange = (event, value) => {
        setPage(value)
        dispatch(getStudents({ page: value }))
    }
    const headers = ['ID','First Name', 'Last Name', 'CGPA', 'Gender'];
   
    return (
        <>
            <AddButton onClick={() => dispatch(addForm())}>+</AddButton>
            {openAddForm ? (<AddStudent />) : ('')}
            
            {students.results?.length > 0
                ?
                (
                    <TableContainer>
                        <Table>
                            <thead style={{ borderRadius: "10px" }}>
                                <TableRow>
                                    {headers.map((header, index) => (
                                        <TableHeader key={index}>{header}</TableHeader>
                                    ))}
                                </TableRow>
                            </thead>
                            <tbody>
                                {students.results.map((student, index) => (
                                    <TableRow key={index}>
                                        <TableCell >{student.id}</TableCell>
                                        <TableCell >{student.fname}</TableCell>
                                        <TableCell >{student.lname}</TableCell>
                                        <TableCell >{student.cgpa}</TableCell>
                                        <TableCell >{student.gender}</TableCell>

                                    </TableRow>
                                ))}
                            </tbody>
                        </Table>
                        <Pagination count={students.total_pages} color="primary" page={students.page} onChange={handlePageChange} />
                    </TableContainer>

                ) :
                ('')
            }

            {students.length == 0 && <h2>No Students</h2>}
            {error && !loading && <p>{error}</p>}
        </>
    )
}

export default Students;