
import React from 'react';
import Students from './components/StudentsComponent';

const App = () => {
    return (
        <div>
            <h2 className='app-title'>Students Lists</h2>
            <hr />
            <Students />
        </div>
     );
}

export default App; 