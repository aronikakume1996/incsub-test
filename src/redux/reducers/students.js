import * as type from '../types';

const initialState = {
    students: [],
    loading: false,
    error: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case type.GET_STUDENTS_REQUESTED:
            return {
                ...state,
                loading: true,
            }

        case type.GET_STUDENTS_SUCCESS:
            return {
                ...state,
                loading: false,
                students: action.students,
            }
        case type.GET_STUDENTS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.message,
            }
        case type.OPEN_ADD_FORM:
            return {
                ...state,
                openAddForm: true,
                openEditForm: false,
            };
        case type.CREATE_STUDENT_REQUESTED:
            return {
                ...state,
                openAddForm: true,
                students: action.payload
            };
        case type.CLOSE_OPEN_FORM:
            return {
                ...state,
                openAddForm: false,
                openEditForm: false,
            };
        default:
            return state;
    }
}