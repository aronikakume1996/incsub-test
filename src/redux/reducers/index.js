import {combineReducers} from 'redux';
import students from './students';

const rootReducer = combineReducers({
    students:students
});

export default rootReducer;