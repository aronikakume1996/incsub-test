import {

    GET_STUDENTS_FAILURE,
    GET_STUDENTS_REQUESTED,
    GET_STUDENTS_SUCCESS,
    CREATE_STUDENT_REQUESTED,
    OPEN_ADD_FORM,
    OPEN_EDIT_FORM,
    CLOSE_EDIT_FORM,
    CLOSE_OPEN_FORM,
} from '../types';

export function getStudents(page) {
    return {
        type: GET_STUDENTS_REQUESTED,
        payload: page
    }
}

export const addForm = () => {
    return {
        type: OPEN_ADD_FORM,
    };
};
export const addStudent = (student) => {
    return {
        type: CREATE_STUDENT_REQUESTED,
        payload: student
    }
}
export const closeAddForm = () => {
    return {
        type: CLOSE_OPEN_FORM,
    };
};
