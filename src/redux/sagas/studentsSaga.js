import { call, put, takeEvery } from 'redux-saga/effects';
import { createStudent, getStudents } from './api';
import { CREATE_STUDENT_REQUESTED, GET_STUDENTS_FAILURE, GET_STUDENTS_REQUESTED, GET_STUDENTS_SUCCESS, STUDENT_FETCH_REQUESTED } from '../types';

function* fetchStudents({payload}) {
    try {
        console.log("Hello",payload.page);
        const students = yield getStudents(payload.page);
        console.log(students);
        yield put({ type: GET_STUDENTS_SUCCESS, students: students })
    } catch (e) {
        yield put({ type: GET_STUDENTS_FAILURE, message: e.message })
    }
}

function* createStudentAction({ payload }) {
	yield createStudent(payload);
	yield put({ type: GET_STUDENTS_REQUESTED, payload: {page:1}});
}

function* studentSaga() {
    yield takeEvery(GET_STUDENTS_REQUESTED, fetchStudents);
    yield takeEvery(CREATE_STUDENT_REQUESTED, createStudentAction);
}

export default studentSaga;