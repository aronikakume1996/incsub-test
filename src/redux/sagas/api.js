const apiUrl = "http://localhost:7004/wp-json/v1/students";
const addApiUrl = "http://localhost:7004/wp-json/v1/students/add";

export const getStudents = async (page=1) => {
    console.log('Paged students', page);
    try {
        const response = await fetch(`${apiUrl}/?page=${page}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        });
        return await response.json();
    } catch (error) {
        throw error;
    }
}

export const createStudent = (student) => {
    const { fname, lname, cgpa, gender } = student;
    fetch(`${apiUrl}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            fname,
            lname,
            cgpa,
            gender

        }),
    })
        .then((response) => response.json())
        .then((data) => {
            getStudents();
            console.log(data);
        }).catch((error) => {
            logError('error', error);
        });
}