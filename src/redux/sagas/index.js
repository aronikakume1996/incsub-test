import {all} from 'redux-saga/effects';
import studentSaga from './studentsSaga';

export default function* rootSaga() {
    yield all([
        studentSaga()
    ])
}