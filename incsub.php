<?

/**
 * @package incsub
 */


/**
 * Plugin Name: Incsub
 * Plugin URI: https://incsub.com/
 * Description: Incsub  is a plugin that designed to be responsive, ensuring that the registration form works seamlessly across various devices, including desktops, tablets, and smartphones.
 * Requires at least: 5.0
 * Requires PHP: 7.0
 * Version: 1.0.0
 * Author: Incsub
 * Author URI: https://incsub.com/
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: incsub
 */

//  If this file is called directly, abort.
defined('ABSPATH') or die('No script kiddies please!');

// Require once the Composer Autoload
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

use Inc\Base\Activate;
use Inc\Base\Deactivate;


/**
 * The code that runs during plugin activation
 * 
 */

function activate_incsub()
{
    Activate::activate();
    Activate::create_table();
    Activate::insert_data();
}

// /**
//  * The code that runs during plugin deactivation
//  * 
//  */
function deactivate_incsub()
{
    Deactivate::deactivate();
}

register_activation_hook(__FILE__, 'activate_incsub');
register_deactivation_hook(__FILE__, 'deactivate_incsub');



// /**
//  * Initialize all the core classes of the plugin
//  * 
//  */


if (class_exists('Inc\\Init')) {
    Inc\Init::register_services();
}
