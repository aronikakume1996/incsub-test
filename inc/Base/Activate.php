<?php

/**
 * @package incsub
 */

namespace Inc\Base;

class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
    }

    public static function create_table()
    {
        global $wpdb;
        global $jal_db_version;

        $table_name = $wpdb->prefix . 'incsub';

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
                id int NOT NULL AUTO_INCREMENT,
                fname VARCHAR(50),
                lname VARCHAR(50),
                cgpa float NOT NULL,
                gender VARCHAR(1000),
                date_created datetime NOT NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);

        add_option('jal_db_version', $jal_db_version);
    }

    public static function insert_data()
    {
        $data = [
            [
                "fname" => "Mebratu",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"

            ],
            [
                "fname" => "Getachew ",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Melkamu",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Alem",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Haymo",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Yared",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"

            ],
            [
                "fname" => "Milinium",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Heron",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "male"
            ],
            [
                "fname" => "Ardani",
                "lname" => "Kumera",
                "cgpa" => "3.2",
                'gender' => "male"
            ],
            [
                "fname" => "Babo",
                "lname" => "Yigzaw",
                "cgpa" => "3.6",
                'gender' => "female"
            ],
            [
                "fname" => "Mebratu",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"

            ],
            [
                "fname" => "Getachew ",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"
            ],
            [
                "fname" => "Melkamu",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"
            ],
            [
                "fname" => "Alem",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"
            ],
            [
                "fname" => "Haymo",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"
            ],
            [
                "fname" => "Yared",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "female"

            ],
            [
                "fname" => "Milinium",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "other"
            ],
            [
                "fname" => "Heron",
                "lname" => "Kumera",
                "cgpa" => "3.5",
                'gender' => "other"
            ],
            [
                "fname" => "Ardani",
                "lname" => "Kumera",
                "cgpa" => "3.2",
                'gender' => "other"
            ],
            [
                "fname" => "Babo",
                "lname" => "Yigzaw",
                "cgpa" => "3.6",
                'gender' => "other"
            ]
        ];
        global $wpdb;
        $table = $wpdb->prefix . 'incsub';
        $count = $wpdb->get_var("SELECT COUNT(*) FROM $table");
        // Check if the table has data
        if ($count > 0) {
            return;
        }
        foreach ($data as $key => $value) {
            $sql = array(
                'fname' => esc_attr__($value['fname'], 'incsub'),
                'lname' => esc_attr__($value['lname'], 'incsub'),
                'cgpa' => esc_attr__($value['cgpa'], 'incsub'),
                'gender' => esc_attr__($value['gender'], 'incsub'),
                'date_created' => esc_attr__(date('Y-m-d H:i'), 'incsub'),
            );
            $list = $wpdb->insert($table, $sql);
        }

        return $list;
    }
}
