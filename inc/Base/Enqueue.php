<?php

/**
 * 
 * @package incsub
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
    public function register()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
    }

    function enqueue()
    {
        wp_enqueue_script( 'incsub-script', $this->plugin_url . 'build/index.js', array( 'wp-element' ), '1.0.0', true );
    }
}
