<?php

/**
 * @package incsub
 */

namespace Inc;


final class Init
{
    /**
     * Initialize the classes
     * 
     */
    public static function get_services()
    {
        return [
            Base\Enqueue::class,
            Api\Api::class,
            Api\Callbacks\Shortcode::class,
        ];
    }

    /**
     * Loop through the classes, initialize them, and call the register() method if it exists
     * 
     */
    public static function register_services()
    {
        foreach (self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }

    /**
     * Initialize the class
     * 
     * @param class $class class from the services array
     * @return class instance new instance of the class
     */
    private static function instantiate($class)
    {
        $service = new $class();
        return $service;
    }
}
