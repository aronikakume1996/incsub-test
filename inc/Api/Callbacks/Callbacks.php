<?php

/**
 * @package Incsub
 */

namespace Inc\Api\Callbacks;

class Callbacks
{
    const NAMESPACE = 'v1/';
    const ROUTE = 'students';

    public function get_students()
    {
        register_rest_route(self::NAMESPACE, self::ROUTE, array(
            'methods'    => 'GET',
            'permission_callback' => '__return_true',
            'callback' => [$this, 'get_students_callback'],
            'args' => array(
                'page' => array(
                    'validate_callback' => function ($param, $request, $key) {
                        return is_numeric($param) && intval($param) > 0;
                    },
                ),
                'per_page' => array(
                    'validate_callback' => function ($param, $request, $key) {
                        return is_numeric($param) && intval($param) > 0;
                    },
                ),
                'orderby' => array(
                    'validate_callback' => function ($param, $request, $key) {
                        return in_array($param, array('fname', 'lname', 'cgpa'));
                    },
                ),
            ),
        ));
    }
    // getter method
    public function get_students_callback($data)
    {
        global $wpdb;

        $page = isset($data['page']) ? intval($data['page']) : 1;
        $per_page = isset($data['per_page']) ? intval($data['per_page']) : 10;
        $orderby = isset($data['orderby']) ? $data['orderby'] : 'date_created';
        $table = $wpdb->prefix . 'incsub';
        $offset = ($page - 1) * $per_page;

        // Query without LIMIT to get the total count
        $total_query = "SELECT COUNT(*) FROM {$table}";
        $total_items = $wpdb->get_var($total_query);

        $query = "SELECT * FROM {$table}
            ORDER BY $orderby DESC
            LIMIT $per_page OFFSET $offset";

        $results = $wpdb->get_results($query);

        $response = array();
        $data = array();

        foreach ($results as $result) {
            $data[] = array(
                'id' => $result->id,
                'fname' => $result->fname,
                'lname' => $result->lname,
                'cgpa' => $result->cgpa,
                'gender' => $result->gender
            );
        }

        $response = array(
            'page' => $page,
            'per_page' => $per_page,
            'orderby' => $orderby,
            'total_pages' => ceil($total_items / $per_page),
            'total_results' => $total_items,
            'results' => $data,
        );

        return $response;
    }

    // Insert a new data to the database
    public function add_student()
    {
        register_rest_route(self::NAMESPACE, self::ROUTE, array(
            'methods' => 'POST',
            'callback' => [$this, 'insert_student_callback'],
            'permission_callback' => '__return_true', 
        ));
    }

    // Callback function for insert_student_callback
    public function insert_student_callback($data)
    {
        global $wpdb;

        // Assuming $data contains the necessary parameters
        // and sanitizes the parameters before inserting into the database
        $fname = sanitize_text_field($data['fname']);
        $lname = sanitize_text_field($data['lname']);
        $cgpa = floatval($data['cgpa']);
        $gender = sanitize_text_field($data['gender']);
        $error = [];

        $table = $wpdb->prefix . 'incsub';

        if (empty($fname) || empty($lname) || empty($cgpa) || empty($gender)) {
            array_push($error, 'Data required.');
            return $error;
        }




        // Insert data into the table
        $wpdb->insert(
            $table,
            array(
                'fname' => $fname,
                'lname' => $lname,
                'cgpa' => $cgpa,
                'gender' => $gender,
                'date_created' => date('Y-m-d H:i:s'),
            )
        );

        $response = array(
            'success' => true,
            'message' => 'Data inserted successfully.',
        );

        return rest_ensure_response($response);
    }
}
