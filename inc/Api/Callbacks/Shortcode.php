<?php

/**
 * @package Incsub
 */

namespace Inc\Api\Callbacks;

use Inc\templates\App;


class Shortcode 
{
    public $template;

    public function register()
    {
        add_shortcode('incsub_students', [$this, 'shortcode_callback']);
    }
    public function shortcode_callback()
    {
        $this->template = new App();
        return $this->template->template();
    }
}
