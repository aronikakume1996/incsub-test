<?php

/**
 * @package Incsub
 */

namespace Inc\Api;

use Inc\Api\Callbacks\Callbacks;

class Api
{
    public $callbacks;


    // REST API for students
    public function register()
    {
        $this->callbacks = new Callbacks();
        add_action('rest_api_init', [$this->callbacks, 'get_students']);
        add_action('rest_api_init', [$this->callbacks, 'add_student']);
    }
}
